package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"os/exec"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"

	_ "github.com/go-sql-driver/mysql"
)

// var db *sql.DB
// var err error

var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

var (
	databaseUsername string
	databasePassword string
)

func getUserName(request *http.Request) (userName string) {
	if cookie, err := request.Cookie("session"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
			userName = cookieValue["name"]
		}
	}
	return userName
}

func setSession(userName string, w http.ResponseWriter) {
	value := map[string]string{
		"name": userName,
	}
	if encoded, err := cookieHandler.Encode("session", value); err == nil {
		cookie := &http.Cookie{
			Name:  "session",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(w, cookie)
	}
}

func clearSession(w http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "session",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(w, cookie)
}

func loginHandler(w http.ResponseWriter, request *http.Request) {
	name := request.FormValue("name")
	pass := request.FormValue("password")
	// fmt.Fprintln(w, name, " ", pass)
	redirectTarget := "/"
	if name != "" && pass != "" {
		// .. check credentials ..
		// db, err := sql.Open("mysql", "root:tz70xxkeq@unix(/var/lib/mysql/mysql.sock)/base")
		db, err := sql.Open("mysql", "root:che0Vooda@/base")
		if err != nil {
			panic(err)
		}
		defer db.Close()

		err = db.Ping()
		if err != nil {
			panic(err.Error())
		}
		// fmt.Println("Connect to MySQL")

		err = db.QueryRow("SELECT username, password FROM users WHERE username=?",
			name).Scan(&databaseUsername, &databasePassword)
		if err != nil {
			http.Redirect(w, request, redirectTarget, 302)
			return
		}
		if pass == databasePassword {
			setSession(name, w)
			redirectTarget = "/admin"
		}
	}
	http.Redirect(w, request, redirectTarget, 302)
}

func logoutHandler(w http.ResponseWriter, request *http.Request) {
	clearSession(w)
	http.Redirect(w, request, "/", 302)
}

func internalPageHandler(w http.ResponseWriter, request *http.Request) {
	w.Header().Set("Content-type", "text/html")
	t, _ := template.ParseFiles("./views/admin.html")
	t.Execute(w, nil)
	userName := getUserName(request)
	if userName != "" {
		fmt.Fprintf(w, "UserName: %s", databaseUsername)
	} else {
		http.Redirect(w, request, "/", 302)
	}
}

// Функция перезапуска
func rebootTomcat(w http.ResponseWriter, request *http.Request) {
	w.Header().Set("Content-type", "text/html")
	t, _ := template.ParseFiles("./views/reboot.html")
	t.Execute(w, nil)

	// Перезапускаем tomcat 8
	cmd, err := exec.Command("/bin/bash", "-c", "tomcatdown && tomcatup").Output()
	if err != nil {
		// fmt.Fprintln(w, "Error: ", err)
		fmt.Fprintln(w, "Не удалось перезапусить!")
	}
	fmt.Fprintf(w, " %s", cmd)
	// Выводим новый PID процесса после Перезапуска
	newcmd, err := exec.Command("/bin/bash", "-c", "pgrep -l java | awk '{print$1}'").Output()
	if err != nil {
		fmt.Fprintln(w, "Error: ", err)
	}
	fmt.Fprintf(w, "New Tomcat PID: %s", newcmd)
}

func main() {

	// Инициализируем gorilla/mux роутер
	router := mux.NewRouter()

	// Страница по умолчанию для нашего сайта это простой html.
	router.Handle("/", http.FileServer(http.Dir("./views/")))

	router.HandleFunc("/login", loginHandler).Methods("POST")
	router.HandleFunc("/logout", logoutHandler).Methods("POST")

	router.HandleFunc("/admin", internalPageHandler)
	router.HandleFunc("/reboot", rebootTomcat)

	// Статику (картинки, скрипты, стили) будем раздавать
	// по определенному роуту /static/{file}
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/",
		http.FileServer(http.Dir("./static/"))))
	http.ListenAndServe(":8989", handlers.LoggingHandler(os.Stdout, router))
}
